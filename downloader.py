from threading import Thread
from multiprocessing import Process
import os
import sys
from queue import Queue



class Worker(Thread):
    def __init__(self, tasks):
        Thread.__init__(self)
        self.tasks = tasks
        self.daemon = True
        self.start()

    def run(self):
        while True:
            func, args, kargs = self.tasks.get()
            try:
                func(*args, **kargs)
            except EOFError as e:
               	print(e)
            finally:
                self.tasks.task_done()



class ThreadPool:
	def __init__(self, num_threads):
		self.tasks = Queue(num_threads)
		for _ in range(num_threads):
			Worker(self.tasks)

	def add_task(self, func, *args, **kargs):
		self.tasks.put((func, args, kargs))

	def add(self, func, args):
		self.add_task(func, args)

	def wait_completion(self):
		self.tasks.join()

